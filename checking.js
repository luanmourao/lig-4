function checkWinner() {
    let map = createMap()

    for (let i = 0; i < map.length; i++) {
        for (let j = 0; j < map[i].length - 3; j++) {

            if (map[i][j] && map[i][j + 1] && map[i][j + 2] && map[i][j + 3]) {

                if (map[i][j].className === map[i][j + 1].className
                    && map[i][j].className === map[i][j + 2].className
                    && map[i][j].className === map[i][j + 3].className) {

                    return true;
                }
            }
        }
    }

    for (let j = 0; j < map[0].length; j++) {
        for (let i = 0; i < map.length - 3; i++) {

            if (map[i][j] && map[i + 1][j] && map[i + 2][j] && map[i + 3][j]) {

                if (map[i][j].className === map[i + 1][j].className
                    && map[i][j].className === map[i + 2][j].className
                    && map[i][j].className === map[i + 3][j].className) {

                    return true;
                }
            }
        }
    }

    for (let i = 0; i < map.length - 3; i++) {
        for (let j = 0; j < map[i].length - 3; j++) {

            if (map[i][j] && map[i + 1][j + 1] && map[i + 2][j + 2] && map[i + 3][j + 3]) {

                if (map[i][j].className === map[i + 1][j + 1].className
                    && map[i][j].className === map[i + 2][j + 2].className
                    && map[i][j].className === map[i + 3][j + 3].className) {

                    return true;
                }
            }

            if (map[i][5 - j] && map[i + 1][4 - j] && map[i + 2][3 - j] && map[i + 3][2 - j]) {
                if (map[i][5 - j].className === map[i + 1][4 - j].className
                    && map[i][5 - j].className === map[i + 2][3 - j].className
                    && map[i][5 - j].className === map[i + 3][2 - j].className) {

                    return true;
                }
            }
        }
    }
}

function checkDraw() {
    for (let i = 0; i < 7; i++) {
        for (let j = 0; j < 6; j++) {
            if (!table.children[i].children[j]) {
                return false
            }
        }
    }
    return true
}

function createMap() {
    let table = document.getElementById("table")
    let map = []

    for (let i = 0; i < 7; i++) {
        map.push([])
        for (let j = 0; j < 6; j++) {
            map[i].push(table.children[i].children[j])
        }
    }

    return map
}